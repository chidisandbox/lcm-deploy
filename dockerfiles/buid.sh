#!/bin/bash

# build logstash image with gelf plugin installed
docker build -t localhost:5000/logstash:5.5.0 -f /dockerfiles/logstash/Dockerfile /dockerfiles/logstash/
docker push localhost:5000/logstash:5.5.0


# build fluentd image with gelf plugin installed
docker build -t localhost:5000/fluentd:v1.1 -f /dockerfiles/fluentd/Dockerfile /dockerfiles/fluentd/
docker push localhost:5000/fluentd:v1.1

# build freeradius image
docker build -t localhost:5000/linkycom/freeradius:1.0.1 -f /dockerfiles/radius/Dockerfile /dockerfiles/radius/
docker push localhost:5000/linkycom/freeradius:1.0.1