---

# Load keepass passwords on keepass connector
- include: NDR-keepass.yml
  vars:
    keepass_keys: "{{referential_keepass_keys}}"

## unitary playbook intended to deploy FreeRadius for LinkyCom
- hosts: radius
  gather_facts: yes
  remote_user: "{{deploy_user}}"

  pre_tasks:
    - set_fact:
        keepass_passwords: "{{hostvars[groups['keepass'][0]].keepass_passwords}}"
      no_log: yes
    - set_fact:
        souche_name: RADIUS
      tags: [envmep,config,configRadius]
    - set_fact:
        config_path: /etc/raddb
        module_log_dir: "{{logical_volumes.ndr_logs.folder_path}}/{{souche_name}}/"
      tags: [config,configRadius]
    - include: NDR-prerequisites.yml
      vars:
        userDefinitions:
          - "{{radius_user}}"
        volumeDefinitions: "{{radius_volumeDefinitions}}"
    - user:
        name: "{{webadm.user}}"
        groups: "{{radius_user.group}}"
        append: yes
      become: yes
      tags: [config,configRadius]

  handlers:
    - name: radiusd restart service
      service:
        name: radiusd
        state: restarted
      become: yes

  roles:
    - role: NDR/nexus/get
      config:
        - "{{radius_module | combine({'artifactId': 'freeradius'}) }}"
        - "{{radius_module | combine({'artifactId': 'freeradius-rest'}) }}"
      tags: [nexusget]

    - role: NDR/module/deploy
      deployables:
        - fileName: "freeradius.{{radius_module.extension}}"
        - fileName: "freeradius-rest.{{radius_module.extension}}"
      tags: [deploy,deployRadius]

    - role: role_iptables
      role_iptables_firewall_allowed_ports:
        - port: 1812
          prot: udp
        - port: 1813
          prot: udp
      tags: [config,configRadius]

    - role: NDR/envmep/doall
      modules_names:
      - "{{souche_name}}"
      allowdeployfiles: True

  tasks:
    - name : create configuration folders
      file:
        path: "{{item}}"
        state: directory
        owner: "{{radius_user.user}}"
        group: "{{radius_user.group}}"
        mode: "0750"
      become: yes
      with_items:
        - "{{config_path}}"
        - "{{config_path}}/mods-enabled"
        - "{{config_path}}/sites-enabled"
        - "{{module_log_dir}}"
      tags: [config,configRadius]

    - name: copy configuration files
      copy:
        src: "files/NDR/radius/static/{{item}}"
        dest: "{{config_path}}/{{item}}"
        owner: "{{radius_user.user}}"
        group: "{{radius_user.group}}"
        mode: "0640"
        directory_mode: "0750"
      become: yes
      notify: radiusd restart service
      with_items:
        - mods-available/
        - mods-config/
        - policy.d/
        - sites-available/
        - dictionary
      tags: [config,configRadius]

    - name: copy configuration templates
      template:
        src: "files/NDR/radius/templates/{{item}}.j2"
        dest: "{{config_path}}/{{item}}"
        owner: "{{radius_user.user}}"
        group: "{{radius_user.group}}"
        mode: "0640"
      become: yes
      notify: radiusd restart service
      with_items:
        - radiusd.conf
        - clients.conf
        - mods-available/linkycom-referential
        - mods-available/linkycom-ticketing
        - mods-available/linkycom-backward-compatibility-logging
        - mods-config/files/authorize
        - policy.d/linkycom-user-password
        - sites-available/linkycom-default
      tags: [config,configRadius]

    - name: enable freeradius modules
      file:
        src: "{{config_path}}/mods-available/{{item}}"
        dest: "{{config_path}}/mods-enabled/{{item}}"
        owner: "{{radius_user.user}}"
        group: "{{radius_user.group}}"
        state: link
      become: yes
      notify: radiusd restart service
      with_items:
        - always
        - attr_filter
        - chap
        - expr
        - files
        - linkycom-referential
        - linkycom-ticketing
        - linkycom-backward-compatibility-logging
        - pap
        - preprocess
      tags: [config,configRadius]

    - name: enable freeradius sites
      file:
        src: "{{config_path}}/sites-available/{{item}}"
        dest: "{{config_path}}/sites-enabled/{{item}}"
        owner: "{{radius_user.user}}"
        group: "{{radius_user.group}}"
        state: link
      become: yes
      notify: radiusd restart service
      with_items:
        - linkycom-default
        - control-socket
      tags: [config,configRadius]

    - name: start freeradius server
      service:
        name: radiusd
        state: started
      become: yes
      tags: [start,startRadius]
