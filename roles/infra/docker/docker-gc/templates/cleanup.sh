#!/bin/bash
#Clean up docker engine - images, containers, networks, volumes

echo "" >> /{{ lcm_local_config_dir }}/docker-gc/cleanup_prune.log
date >> /{{ lcm_local_config_dir }}/docker-gc/cleanup_prune.log
docker system prune -fa >> /{{ lcm_local_config_dir }}/docker-gc/cleanup_prune.log