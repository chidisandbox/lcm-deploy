
"""Ansible callback plugin to print a summary completion status of installation
phases.
"""
from datetime import datetime
from ansible.plugins.callback import CallbackBase
from ansible import constants as C


class CallbackModule(CallbackBase):
    """This callback summarizes installation phase status."""

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'installer_checkpoint'
    CALLBACK_NEEDS_WHITELIST = False

    def __init__(self):
        super(CallbackModule, self).__init__()

    def v2_playbook_on_stats(self, stats):

        # Set the order of the installer phases
        installer_phases = [
            'installer_phase_initialize',
            'installer_phase_docker_infra',
            'installer_phase_glusterfs',
            'installer_phase_portainer',
            'installer_phase_consul',
            'installer_phase_registrator',
            'installer_phase_traefik',
            'installer_phase_logging',
            'installer_phase_monitoring',
            'installer_phase_postgresql',
            'installer_phase_rabbitmq',
            'installer_phase_oracledb',
            'installer_phase_linkycom'
        ]

        # Define the attributes of the installer phases
        phase_attributes = {
             'installer_phase_initialize': {
                'component': 'Facts',
                'title': 'Initialization',
                'playbook': ''
            },
            'installer_phase_docker_infra': {
                'component': 'Infra',
                'title': 'Docker',
                'playbook': 'playbooks/infra/docker/setup.yml'
            },
            'installer_phase_glusterfs': {
                'component': 'Infra',
                'title': 'Glusterfs',
                'playbook': 'playbooks/infra/storage/glusterfs/setup.yml'
            },
            'installer_phase_portainer': {
                'component': 'Infra',
                'title': 'Portainer',
                'playbook': 'playbooks/apps/portainer/setup.yml'
            },
            'installer_phase_consul': {
                'component': 'Infra',
                'title': 'Consul',
                'playbook': 'playbooks/infra/consul/setup.yml'
            },
            'installer_phase_traefik': {
                'component': 'Infra',
                'title': 'Traefik',
                'playbook': 'playbooks/infra/traefik/setup.yml'
            },
            'installer_phase_registrator': {
                'component': 'Infra',
                'title': 'Gliderlab-registrator',
                'playbook': 'playbooks/infra/registrator/setup.yml'
            },
            'installer_phase_logging': {
                'component': 'Logging',
                'title': 'Logging Stack',
                'playbook': 'playbooks/logging/setup.yml'
            },
            'installer_phase_monitoring': {
                'component': 'Monitoring',
                'title': 'Monitoring Stack',
                'playbook': 'playbooks/monitoring/setup.yml'
            },
            'installer_phase_postgresql': {
                'component': 'Database',
                'title': 'Database Stack',
                'playbook': 'playbooks/database/setup.yml'
            },
            'installer_phase_rabbitmq': {
                'component': 'Middleware',
                'title': 'RabbitMQ',
                'playbook': 'playbooks/middleware/rabbitmq/setup.yml'
            },
            'installer_phase_oracledb': {
                'component': 'Middleware',
                'title': 'OracleDb',
                'playbook': 'playbooks/middleware/oracledb/setup.yml'
            },
            'installer_phase_linkycom': {
                'component': 'Apps',
                'title': 'Linkycom',
                'playbook': 'playbooks/apps/linkycom/setup.yml'
            }

        }

        # Find the longest phase title
        max_title_column = 0
        max_component_column = 0
        for phase in phase_attributes:
            max_title_column = max(max_title_column, len(phase_attributes[phase]['title']))
            max_component_column = max(max_component_column, len(phase_attributes[phase]['component']))

        if '_run' in stats.custom:
            self._display.banner('INSTALLER STATUS')
            for phase in installer_phases:
                phase_component = phase_attributes[phase]['component']
                phase_title = phase_attributes[phase]['title']
                title_padding = max_title_column - len(phase_title) + 2
                component_padding = max_component_column - len(phase_component) + 2
                nbtab = (max_title_column - len(phase_title)) / 6  + 2
                if phase in stats.custom['_run']:
                    phase_status = stats.custom['_run'][phase]['status']
                    phase_time = phase_time_delta(stats.custom['_run'][phase])
                    #icon = u'\u2714'.encode('ascii') if phase_status =='Complete' else ' ' if phase_status == 'Not Started' else u'\u2714'.encode('ascii')
                    icon = ''
                    self._display.display(
                        '{}{}|\t{}{}: {}{} ({})'.format(phase_component,' ' * component_padding,phase_title , ' ' * title_padding, icon, phase_status, phase_time),
                        color=self.phase_color(phase_status))
                    if phase_status == 'In Progress' and phase != 'installer_phase_initialize':
                        self._display.display(
                            '\tThis phase can be restarted by running: {}'.format(
                                phase_attributes[phase]['playbook']))
                else:
                    # Phase was not found in custom stats
                    self._display.display(
                        '{}{}|\t{}{}: {}'.format(phase_component, ' ' * component_padding ,phase_title,' ' * title_padding,'' + 'Not Started'),
                        color=C.COLOR_SKIP)

        self._display.display("", screen_only=True)

    def phase_color(self, status):
        """ Return color code for installer phase"""
        valid_status = [
            'In Progress',
            'Complete',
        ]

        if status not in valid_status:
            self._display.warning('Invalid phase status defined: {}'.format(status))

        if status == 'Complete':
            phase_color = C.COLOR_OK
        elif status == 'In Progress':
            phase_color = C.COLOR_ERROR
        else:
            phase_color = C.COLOR_WARN

        return phase_color


def phase_time_delta(phase):
    """ Calculate the difference between phase start and end times """
    time_format = '%Y%m%d%H%M%SZ'
    phase_start = datetime.strptime(phase['start'], time_format)
    if 'end' not in phase:
        # The phase failed so set the end time to now
        phase_end = datetime.now()
    else:
        phase_end = datetime.strptime(phase['end'], time_format)
    delta = str(phase_end - phase_start).split(".")[0]  # Trim microseconds

    return delta
