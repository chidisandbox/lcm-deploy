---
- name: Populate config host groups
  hosts: localhost
  connection: local
  become: no
  gather_facts: no
  tasks:
  - name: Load group name mapping variables
    include_vars: vars/cluster_hosts.yml

  - name: Evaluate groups | g_docker_hosts
    fail:
      msg: This playbook requires g_docker_hoststo be set
    when: g_docker_hosts is not defined

  - name: Evaluate groups | g_swarm_hosts
    fail:
      msg: This playbook requires g_swarm_hosts to be set
    when: g_swarm_hosts is not defined

  - name: Evaluate groups | g_swarm_manager_hosts
    fail:
      msg: This playbook requires g_swarm_manager_hosts to be set
    when: g_swarm_manager_hosts is not defined

  - name: Evaluate groups | g_swarm_worker_hosts required
    fail:
      msg: This playbook requires g_swarm_worker_hosts to be set
    when: g_swarm_worker_hosts is not defined

  - name: Evaluate groups | g_glusterfs_hosts required
    fail:
      msg: This playbook requires g_glusterfs_hosts to be set
    when: g_glusterfs_hosts is not defined

  - name: Evaluate groups | g_consul_hosts required
    fail:
      msg: This playbook requires g_consul_hosts to be set
    when: g_consul_hosts is not defined

  - name: Evaluate groups | g_consul_server_hosts required
    fail:
      msg: This playbook requires g_consul_server_hosts to be set
    when: g_consul_server_hosts is not defined

  - name: Evaluate groups | g_consul_client_hosts required
    fail:
      msg: This playbook requires g_consul_client_hosts to be set
    when: g_consul_client_hosts is not defined

  - name: Evaluate lcm_all_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_all_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_all_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_docker_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_docker_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_docker_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_swarm_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_swarm_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_swarm_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_swarm_manager_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_swarm_manager_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_swarm_manager_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_swarm_worker_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_swarm_worker_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_swarm_worker_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_first_swarm_manager
    add_host:
      name: "{{ g_swarm_manager_hosts[0] }}"
      groups: lcm_first_swarm_manager
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    when: g_swarm_manager_hosts|length > 0
    changed_when: no

  - name: Evaluate lcm_glusterfs
    add_host:
      name: "{{ item }}"
      groups: lcm_glusterfs
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_glusterfs_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_consul_managers
    add_host:
      name: "{{ item }}"
      groups: lcm_consul_managers
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_consul_server_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_first_consul_manager
    add_host:
      name: "{{ g_consul_server_hosts[0] }}"
      groups: lcm_first_consul_manager
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    when: g_consul_server_hosts|length > 0
    changed_when: no

  - name: Evaluate lcm_consul_workers
    add_host:
      name: "{{ item }}"
      groups: lcm_consul_workers
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_consul_client_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_rabbitmq_hosts
    add_host:
      name: "{{ item }}"
      groups: lcm_rabbitmq_hosts
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    with_items: "{{ g_rabbitmq_hosts | default([]) }}"
    changed_when: no

  - name: Evaluate lcm_rabbitmq_master
    add_host:
      name: "{{ g_rabbitmq_hosts[0] }}"
      groups: lcm_rabbitmq_master
      ansible_ssh_user: "{{ g_ssh_user | default(omit) }}"
      ansible_become: "{{ g_sudo | default(omit) }}"
    when: g_rabbitmq_hosts|length > 0
    changed_when: no
  tags:
    - always