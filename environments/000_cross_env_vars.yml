---

crontab_file: /etc/crontab
# lcm config
lcm_local_config_dir: /opt/lcm-deploy

# docker
docker_compose_version: "3.3"
use_docker_storage: true
docker_gc_cron_timing: "*/10 * * * *"
docker_gc_cron_user: root

docker_router_network_name: lcm-router-network
docker_postgresql_network_name: lcm-postgresql-network
docker_logging_network_name: lcm-logging-network
docker_linkycom_network_name: linkycom-network
docker_engine_metrics_port: 9323

# GlusterFs
glusterfs_service_name: glusterfs
glusterfs_service_image: "{{ docker_lcm_registry }}/gluster/gluster-centos:gluster3u9_centos7"

# Heketi
heketi_service_name: heketi
heketi_service_image: "{{ docker_lcm_registry }}/heketi/heketi:5"

# Consul
consul_server_service_name: consul-server
consul_server_service_image: "{{ docker_lcm_registry }}/consul:1.0.2"
consul_bind_interface: enp0s8

consul_client_service_name: consul-client
consul_client_service_image: "{{ docker_lcm_registry }}/consul:1.0.2"

# PORTAINER
portainer_service_name: portainer

# Traefik
traefik_config_base_directory: /opt/infra/traefik
traefik_service_name: traefik
traefik_service_image: "{{ docker_lcm_registry }}/traefik:cancoillotte"
traefik_docker_domain: linkycom.local
traefik_consul_catalog_domain: "{{ traefik_docker_domain }}"
traefik_web_ui_port: 8080
traefik_http_entrypoint_port: 80

# Logstash
logstash_service_name: logstash
logstash_service_image: "{{ docker_lcm_registry }}/logstash:5.5.0"

# Fluentd
fluentd_service_name: fluentd
fluentd_service_image: "{{ docker_lcm_registry }}/fluentd:v1.1"

# Graylog
graylog_service_name: graylog
graylog_service_image: "{{ docker_lcm_registry }}/graylog/graylog:2.4.3-1"
graylog_journal_enabled: false # disabled due to unknown issue , maybe with docker or vm
graylog_journal_volume_name: graylog-journal

# Graylog Mongodb
graylog_mongodb_service_name: graylog-mongodb
graylog_mongodb_service_image: "{{ docker_lcm_registry }}/mongo:3"
graylog_deploy_from_stack: true
graylog_mongodb_volume_name: graylog-mongodb

# Graylog Elasticsearch
graylog_es_service_name: graylog-es
graylog_es_service_image: "{{ docker_lcm_registry }}/elasticsearch:5.5.1"
graylog_es_volume_name: graylog-es

# CAdvisor
cadvisor_service_name: cadvisor
cadvisor_service_image: "{{ docker_lcm_registry }}/google/cadvisor:v0.28.3"
cadvisor_http_port: 8090

# Node Exporter
node_exporter_service_name: node-exporter
node_exporter_service_image: "{{ docker_lcm_registry }}/prom/node-exporter:v0.14.0"

# Rabbitmq Exporter
rabbitmq_exporter_service_name: rabbitmq-exporter
rabbitmq_exporter_service_image: "{{ docker_lcm_registry }}/kbudde/rabbitmq-exporter:v0.25.2"

# alert manager
alert_manager_service_name:  alertmanager
alert_manager_service_image: "{{ docker_lcm_registry }}/prom/alertmanager:v0.14.0"
alertmanager_volume_name: alertmanager

# prometheus
prometheus_service_name: prometheus
prometheus_service_image: "{{ docker_lcm_registry }}/prom/prometheus:v2.1.0"
prometheus_volume_name: prometheus

# Grafana
grafana_service_name: grafana
grafana_service_image: "{{ docker_lcm_registry }}/grafana/grafana:5.0.0"
grafana_volume_name: grafana

# Gluster configuration.
docker_volume_use_gluster: true
gluster_mount_dir: /mnt/gluster
gluster_brick_dir: /srv/gluster/brick

# Rabbitmq
rabbitmq_server_version: 3.6.1

# Oracle DB
oracledb_service_name: oracledb
oracledb_service_image: "{{ docker_lcm_registry }}/wnameless/oracle-xe-11g"

# PostgreSQL DB
postgresql_service_name: postgresql
postgresql_service_image: "{{ docker_lcm_registry }}/postgres:9.6"
postgresql_volume_name: postgresql

# PGAdmin
pgadmin_service_name: pgadmin
pgadmin_service_image: "{{ docker_lcm_registry }}/chorss/docker-pgadmin4:2.1"
pgadmin_volume_name: pgadmin

# PostgreSQL prometheus exporter
postgres_exporter_service_name: postgres_exporter
postgres_exporter_service_image: "{{ docker_lcm_registry }}/wrouesnel/postgres_exporter:v0.4.1"
