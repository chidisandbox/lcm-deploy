#!/bin/bash

set -e
shopt -s extglob

#
# Vagrant provisionning script
#
# Usage for provisionning VM & running (in Vagrant file):
#
# script.sh --install <NNA> <playbook>
#
# e.g. :
# script.sh --install NDR NDR-ACCUEIL
#
# Usage for running only (from host):
#
# vagrant ssh -c ./specs
#

fix_ndr_issues () {

  echo 'Fix NDR issues'
  echo 'None'

}

fix_ndr_conf_issues () {

  echo 'Fix NDR conf issues'
  echo 'None'

}

fix_vagant_image_issues () {
  echo 'Fix vagrant image issues'
  echo 'None'
}

nexus_get () {

  local output=$1
  local groupId=$2
  local artifactId=$3
  local version=$4
  local repo=$5
  local classifier=$6
  local atype=$7

  wget -q -O ${output} "http://nexus.integration.linky.distribution.edf.fr/service/local/artifact/maven/content?g=${groupId}&a=${artifactId}&v=${version}&r=${repo}&c=${classifier}&e=${atype}" || true

}

link () {
  local to=${@: -1}
  if [ -d $to ]; then
    for tgt in ${@:1:$#-1}; do
      local name=$(basename "$tgt")
      [ -e $tgt ] && rm -rf $to/$name && ln -s $tgt $to/
    done
  elif [ $# == 2 ]; then
    local tgt=$1
    [ -e $tgt ] && rm -rf $to && ln -s $tgt $to
  else
    echo "Linking error [${@}]" 1>&2
  fi
}

link_tree () {
  local tgt=$1
  local to=$2
  if [ -d $tgt -a -d $to ]; then
    for file in $(find $tgt -maxdepth 1 ! -type d -printf '%P\n'); do
      [ -e $tgt/$file ] && rm -f $to/$file && ln -s $tgt/$file $to/
    done
    for dir in $(find $tgt -depth -type d -printf '%P\n'); do
      mkdir -p $to/$dir
      for file in $(find $tgt/$dir -maxdepth 1 ! -type d -printf '%P\n'); do
        [ -e $tgt/$dir/$file ] && rm -f $to/$dir/$file && ln -s $tgt/$dir/$file $to/$dir/
      done
    done
  else
    echo "Linking error [${@}]" 1>&2
  fi
}

install_ansibox () {

  mkdir -p ~/ansibox/{files,roles,group_vars,host_vars,library,action_plugins,lookup_plugins,callback_plugins,connection_plugins,filter_plugins,vars_plugins,tests} || true

}

fix_hosts () {

  local rolename=$1

  echo "Copy hosts files (to avoid rolespec modification in sources)"
  if [ "$rolename" == "all" ]; then
    rm -f ~/ansibox/hosts*
    cp /ansibox_ndr_conf/hosts* ~/ansibox || true
  else
    rm -f ~/ansibox/tests/playbooks/${rolename}/inventory/hosts*
    cp /ansibox_ndr/tests/playbooks/${rolename}/inventory/hosts* ~/ansibox/tests/playbooks/${rolename}/inventory || true
  fi

}

install_ansibox_ndr () {

  local rolename=$1

  echo "Install ansibox_ndr"

  # Role path NEED to be a real folder (and not behind a symlink)
  mkdir -p ~/ansibox/roles/playbooks/${rolename} || true
  link_tree /ansibox_ndr ~/ansibox/ || true
  rm ~/ansibox/ansible.cfg

  fix_ndr_issues

}

install_ansibox_ndr_conf () {

  echo "Install ansibox_ndr_conf"

  link_tree /ansibox_ndr_conf ~/ansibox/

  fix_ndr_conf_issues

}

link_galaxy() {

  local rolename=$1

  if [ -e ~/ansibox/${rolename%.*}-requirements.yml ]; then
    echo "Install Galaxy requirements"
    link ~/ansibox/${rolename%.*}-requirements.yml ~/ansibox/tests/playbooks/${rolename}/playbooks/test-requirements.yml || true
  fi

}

install_ansibox_ndr_test () {

  local rolename=$1

  echo "Install ansibox_ndr_test"

  mkdir -p ~/ansibox/tests/playbooks/

  link_tree /ansibox_ndr/tests/playbooks ~/ansibox/tests/playbooks/ || true
  if [ ! -e ~/ansibox/tests/playbooks/${rolename}/playbooks/test.yml ]; then
    mkdir -p ~/ansibox/tests/playbooks/${rolename}/playbooks/
    ln -s ~/ansibox/${rolename}.yml ~/ansibox/tests/playbooks/${rolename}/playbooks/test.yml || true
  fi

  link_galaxy "${rolename}"

  for folder in group_vars host_vars; do
    if [ -e ~/ansibox/tests/playbooks/${rolename}/${folder} ]; then
      link_tree ~/ansibox/tests/playbooks/${rolename}/${folder} ~/ansibox/${folder}
    fi
  done

  # Il y a un bug Ansible quand la play n'est pas à la racine, les fichiers group_vars, files etc ne sont pas trouvés.
  link ~/ansibox/!(tests) ~/ansibox/tests/playbooks/${rolename}/playbooks/ || true
  link ~/ansibox/NDR-* ~/ansibox/tests/playbooks/${rolename}/playbooks || true

  fix_hosts $rolename

}

install_rolespec () {

  # Install RoleSpec
  if [ ! -d ~vagrant/rolespec ] ; then
    ROLESPEC_URL="http://nexus.integration.linky.distribution.edf.fr/service/local/artifact/maven/content?r=linkycom-snapshots&g=linkycom.outils&a=rolespec&v=LATEST&e=tar.gz"
    su vagrant -mc "curl -skL '$ROLESPEC_URL' | tar xzf - --directory ~vagrant"
  fi
  cd ~vagrant/rolespec && (umask 0022 && make install)
  /usr/local/bin/rolespec -v | grep '^rolespec'
  cd ~vagrant

  # Wait for integration in vagrant boxes
  cat << EOF >> /home/vagrant/.ssh/config
Host *
    LogLevel    error
EOF
  chmod 600 /home/vagrant/.ssh/config
  chown vagrant: /home/vagrant/.ssh/config

}

if [ "x$1" == "x--vagrant" ]; then

  install_ansibox
  install_ansibox_ndr "$2"
  install_ansibox_ndr_conf
  install_ansibox_ndr_test "$2"
  exit

fi

if [ "x$1" == "x--install" ]; then

  cp ~vagrant/specs /usr/local/bin/specs
  chmod 755 /usr/local/bin/specs
  install_rolespec
  fix_vagant_image_issues
  su vagrant -c "/usr/local/bin/specs --vagrant ${2}"
  sed -i "s|__ROLE__|$2|g" /usr/local/bin/specs || true
  exit

fi

fix_hosts __ROLE__

# Simulate ansible.cfg with envvars (rolespec overrides ansible.cfg from ansibox)
export ANSIBLE_HOST_KEY_CHECKING=false
export ANSIBLE_RETRY_FILES_ENABLED=false
export ANSIBLE_MODULE_LANG=C
export ANSIBLE_CALLBACK_WHITELIST="timer,profile_tasks"

# Force LANG for output parsing in rolespec tests
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

cd ~vagrant/ansibox && rolespec -r playbooks/__ROLE__ "$*"
