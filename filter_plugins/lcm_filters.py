#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=too-many-lines
"""
Custom filters
"""
import json
import os
import pdb
import random
import re

from base64 import b64encode
from collections import Mapping
# pylint no-name-in-module and import-error disabled here because pylint
# fails to properly detect the packages when installed in a virtualenv
from distutils.util import strtobool  # pylint:disable=no-name-in-module,import-error
from distutils.version import LooseVersion  # pylint:disable=no-name-in-module,import-error
from operator import itemgetter

import pkg_resources
import yaml

from ansible import errors


# ansible.compat.six goes away with Ansible 2.4
try:
    from ansible.compat.six import string_types, u
    from ansible.compat.six.moves.urllib.parse import urlparse
except ImportError:
    from ansible.module_utils.six import string_types, u
    from ansible.module_utils.six.moves.urllib.parse import urlparse

def lcm_split(string, separator=','):
    """ This splits the input string into a list. If the input string is
    already a list we will return it as is.
    """
    if isinstance(string, list):
        return string
    return string.split(separator)

def lcm_prepend_strings_in_list(data, prepend):
    """ This takes a list of strings and prepends a string to each item in the
        list
        Ex: data = ['cart', 'tree']
            prepend = 'apple-'
            returns ['apple-cart', 'apple-tree']
    """
    if not isinstance(data, list):
        raise errors.AnsibleFilterError("|failed expects first param is a list")
    if not all(isinstance(x, string_types) for x in data):
        raise errors.AnsibleFilterError("|failed expects first param is a list"
                                        " of strings")
    retval = [prepend + s for s in data]
    return retval

class FilterModule(object):
    """ Custom ansible filter mapping """

    # pylint: disable=no-self-use, too-few-public-methods
    def filters(self):
        """ returns a mapping of filters to methods """
        return {
            "lcm_prepend_strings_in_list": lcm_prepend_strings_in_list,
            "lcm_split": lcm_split
        }