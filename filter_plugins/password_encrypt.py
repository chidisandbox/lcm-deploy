# -*- coding: utf-8 -*-

from ansible import errors
import base64
from Crypto.Cipher import AES


def decode_key(encoded):
    """
    >>> decode_key('EN/BeAezQT2lSwTUZyLiYC66+A+2XzHkiyvqYWwqmRN+')
    ('\\xdf\\xc1x\\x07\\xb3A=\\xa5K\\x04\\xd4g"\\xe2`.', '\\xba\\xf8\\x0f\\xb6_1\\xe4\\x8b+\\xeaal*\\x99\\x13~')
    >>> decode_key('EOlsP02hZ2jc9FUH+q6EL4YkWcKrnFffpb24j3/iCCvt')
    ('\\xe9l?M\\xa1gh\\xdc\\xf4U\\x07\\xfa\\xae\\x84/\\x86', '$Y\\xc2\\xab\\x9cW\\xdf\\xa5\\xbd\\xb8\\x8f\\x7f\\xe2\\x08+\\xed')
    >>> decode_key('EHTV9ojpRJGvHbQQhX4CaEIA12/t6RVnjPJN1g0TZDzA')
    ('t\\xd5\\xf6\\x88\\xe9D\\x91\\xaf\\x1d\\xb4\\x10\\x85~\\x02hB', '\\x00\\xd7o\\xed\\xe9\\x15g\\x8c\\xf2M\\xd6\\r\\x13d<\\xc0')
    >>> decode_key('EDdRFtsl5lQJdqFy/gVFmdfbEqV2LYRTW92lMrLX7/ye')
    ('7Q\\x16\\xdb%\\xe6T\\tv\\xa1r\\xfe\\x05E\\x99\\xd7', '\\xdb\\x12\\xa5v-\\x84S[\\xdd\\xa52\\xb2\\xd7\\xef\\xfc\\x9e')
    """
    raw_key = base64.b64decode(encoded)
    iv_len = ord(raw_key[0])
    iv = raw_key[1:iv_len+1]
    key = raw_key[iv_len+1:]
    return (iv, key)

def pad(text):
    """
    >>> pad('')
    '\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10'
    >>> pad('a')
    'a\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f'
    >>> pad('ab')
    'ab\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e'
    >>> pad('abc')
    'abc\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r'
    >>> pad('abcd')
    'abcd\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c'
    >>> pad('abcde')
    'abcde\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b'
    >>> pad('abcdef')
    'abcdef\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n'
    >>> pad('abcdefg')
    'abcdefg\\t\\t\\t\\t\\t\\t\\t\\t\\t'
    >>> pad('abcdefgh')
    'abcdefgh\\x08\\x08\\x08\\x08\\x08\\x08\\x08\\x08'
    >>> pad('abcdefghi')
    'abcdefghi\\x07\\x07\\x07\\x07\\x07\\x07\\x07'
    >>> pad('abcdefghij')
    'abcdefghij\\x06\\x06\\x06\\x06\\x06\\x06'
    >>> pad('abcdefghijk')
    'abcdefghijk\\x05\\x05\\x05\\x05\\x05'
    >>> pad('abcdefghijkl')
    'abcdefghijkl\\x04\\x04\\x04\\x04'
    >>> pad('abcdefghijklm')
    'abcdefghijklm\\x03\\x03\\x03'
    >>> pad('abcdefghijklmn')
    'abcdefghijklmn\\x02\\x02'
    >>> pad('abcdefghijklmno')
    'abcdefghijklmno\\x01'
    >>> pad('abcdefghijklmnop')
    'abcdefghijklmnop\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10'
    """
    padding_len = AES.block_size - (len(text) % AES.block_size)
    padding = chr(padding_len) * padding_len
    padded = text + padding
    return padded

def unpad(text):
    """
    >>> unpad('\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10')
    ''
    >>> unpad('a\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f\\x0f')
    'a'
    >>> unpad('ab\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e\\x0e')
    'ab'
    >>> unpad('abc\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r\\r')
    'abc'
    >>> unpad('abcd\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c\\x0c')
    'abcd'
    >>> unpad('abcde\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b\\x0b')
    'abcde'
    >>> unpad('abcdef\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n')
    'abcdef'
    >>> unpad('abcdefg\\t\\t\\t\\t\\t\\t\\t\\t\\t')
    'abcdefg'
    >>> unpad('abcdefgh\\x08\\x08\\x08\\x08\\x08\\x08\\x08\\x08')
    'abcdefgh'
    >>> unpad('abcdefghi\\x07\\x07\\x07\\x07\\x07\\x07\\x07')
    'abcdefghi'
    >>> unpad('abcdefghij\\x06\\x06\\x06\\x06\\x06\\x06')
    'abcdefghij'
    >>> unpad('abcdefghijk\\x05\\x05\\x05\\x05\\x05')
    'abcdefghijk'
    >>> unpad('abcdefghijkl\\x04\\x04\\x04\\x04')
    'abcdefghijkl'
    >>> unpad('abcdefghijklm\\x03\\x03\\x03')
    'abcdefghijklm'
    >>> unpad('abcdefghijklmn\\x02\\x02')
    'abcdefghijklmn'
    >>> unpad('abcdefghijklmno\\x01')
    'abcdefghijklmno'
    >>> unpad('abcdefghijklmnop\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10\\x10')
    'abcdefghijklmnop'
    """
    padding_len = ord(text[-1])
    unpadded = text[:-padding_len]
    return unpadded

def encrypt(value, key):
    """
    >>> encrypt('DYNPARKLIVENOC1', 'EN/BeAezQT2lSwTUZyLiYC66+A+2XzHkiyvqYWwqmRN+')
    '95076q1GlpjqbyH7Qr+INA=='
    >>> encrypt('@@DB.PWD.DPK@@', 'EN/BeAezQT2lSwTUZyLiYC66+A+2XzHkiyvqYWwqmRN+')
    'ljEt8d7g4ITI/V3M3vuP7g=='
    >>> encrypt('DYNPARKLIVENOC1', 'EOlsP02hZ2jc9FUH+q6EL4YkWcKrnFffpb24j3/iCCvt')
    'TZKF4zlvjJPH0rEi4HcSYA=='
    >>> encrypt('@@DB.PWD.DPK@@', 'EOlsP02hZ2jc9FUH+q6EL4YkWcKrnFffpb24j3/iCCvt')
    '4psNka13IqhbU9mCjYZIHg=='
    >>> encrypt('@@DB.PWD.REPORT@@', 'EHTV9ojpRJGvHbQQhX4CaEIA12/t6RVnjPJN1g0TZDzA')
    '1vegSc+G4PGWyS/wWM8H52SxHvNWq1YmuWhEjRJUkhA='
    >>> encrypt('TELREPORTNOC1', 'EHTV9ojpRJGvHbQQhX4CaEIA12/t6RVnjPJN1g0TZDzA')
    'fjd59k/4gFnj599B306iDA=='
    >>> encrypt('DYNPARKLIVENOC1', 'EDdRFtsl5lQJdqFy/gVFmdfbEqV2LYRTW92lMrLX7/ye')
    'i41m3mfB72b/4QVlaWngpg=='
    >>> encrypt('dynparklivenoc1', 'EDdRFtsl5lQJdqFy/gVFmdfbEqV2LYRTW92lMrLX7/ye')
    'xmsRAR2JnNZJSAnWbIjsiw=='
    """
    if not key:
        errors.AnsibleFilterError('key argument is mandatory')
    iv, real_key = decode_key(key)
    cipher = AES.new(real_key, AES.MODE_CBC, iv) # AES/CBC/PKCS5Padding
    encrypted = cipher.encrypt(pad(value))
    return base64.b64encode(encrypted)

def decrypt(value, key):
    """
    >>> decrypt('95076q1GlpjqbyH7Qr+INA==', 'EN/BeAezQT2lSwTUZyLiYC66+A+2XzHkiyvqYWwqmRN+')
    'DYNPARKLIVENOC1'
    >>> decrypt('ljEt8d7g4ITI/V3M3vuP7g==', 'EN/BeAezQT2lSwTUZyLiYC66+A+2XzHkiyvqYWwqmRN+')
    '@@DB.PWD.DPK@@'
    >>> decrypt('TZKF4zlvjJPH0rEi4HcSYA==', 'EOlsP02hZ2jc9FUH+q6EL4YkWcKrnFffpb24j3/iCCvt')
    'DYNPARKLIVENOC1'
    >>> decrypt('4psNka13IqhbU9mCjYZIHg==', 'EOlsP02hZ2jc9FUH+q6EL4YkWcKrnFffpb24j3/iCCvt')
    '@@DB.PWD.DPK@@'
    >>> decrypt('1vegSc+G4PGWyS/wWM8H52SxHvNWq1YmuWhEjRJUkhA=', 'EHTV9ojpRJGvHbQQhX4CaEIA12/t6RVnjPJN1g0TZDzA')
    '@@DB.PWD.REPORT@@'
    >>> decrypt('fjd59k/4gFnj599B306iDA==', 'EHTV9ojpRJGvHbQQhX4CaEIA12/t6RVnjPJN1g0TZDzA')
    'TELREPORTNOC1'
    >>> decrypt('i41m3mfB72b/4QVlaWngpg==', 'EDdRFtsl5lQJdqFy/gVFmdfbEqV2LYRTW92lMrLX7/ye')
    'DYNPARKLIVENOC1'
    >>> decrypt('xmsRAR2JnNZJSAnWbIjsiw==', 'EDdRFtsl5lQJdqFy/gVFmdfbEqV2LYRTW92lMrLX7/ye')
    'dynparklivenoc1'
    """
    if not key:
        errors.AnsibleFilterError('key argument is mandatory')
    iv, real_key = decode_key(key)
    cipher = AES.new(real_key, AES.MODE_CBC, iv) # AES/CBC/PKCS5Padding
    raw_value = base64.b64decode(value)
    decrypted = cipher.decrypt(raw_value)
    return unpad(decrypted)

class FilterModule (object):
    def filters(self):
        return {
            'password_encrypt': encrypt,
            'password_decrypt': decrypt,
            }
