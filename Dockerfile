FROM ansible/centos7-ansible:stable
MAINTAINER Linkycom <ERDF-LINKY-LINKYCOM-EQUIPE2>

RUN mkdir /ansible-deploy && mkdir /lcm-inventory
WORKDIR /ansible-deploy

VOLUME /lcm-inventory

ADD ./ /ansible-deploy/